---
title: "DevOps for Python development"
author: "Mike"
date: 2023-11-04
draft: false
toc: true
images:
tags:
  - python
  - devops
---

## Introduction

Science is all about reproducibility. It doesn't matter how ground-breaking your discovery was - if it can't be reproduced, validated, and verified it pretty much didn't happen. I don't envy those that need to describe reproducible protocols for experiments they do in the real world, with real, tangible materials, but we in the data science field have it easy. We write instructions for computers that will execute exactly what we wrote down. If you run the same script twice you should get the same answer. There are nuances like making sure you can provide constant seeds to random number generators so the initial weights of your neural network can be regenerated, or your dataset is split into the same random validation and test sets, but for the most part you have no excuse if you opt not to provide reproducible code.

It goes without saying, because this is a blog after all, but what follows below is a very opinionated example of how you might consider structuring a (python) project repository with tools that facilitate reproducibility. If you're like me, you hate copy-and-pasting code from one repository to another. This example describes a [cookiecutter](https://github.com/cookiecutter/cookiecutter) template that you can use to quickly get a new git repository up and running. Environment isolation is handled with [Docker](https://www.docker.com/get-started/), [pip](https://pypi.org/project/pip/) is used to handle package dependencies with a locking mechanism to provide flexible up-to-date development dependency definitions as well as a separate version locked requirements file. The project skeleton is also complete with [bump2version](https://pypi.org/project/bump2version/) for handling release versioning, a Makefile for common build commands, a gitpod configuration, and gitlab CI/CD recipes. All of this is explained in more detail below - keep reading!

If you want to see a project using this setup in action, take a look at [`sportsml`](https://gitlab.com/mikedoesdatascience/sportsml), a hobby project of mine for working with sports data.


## Setting up a new project with cookiecutter

Before I get into the detailed explanations, let's get a new project repository set up with all of the DevOps tools I mentioned previously. First we'll install cookiecutter itself with 

`python -m pip install cookiecutter` 

and use it to build the skeleton project structure from the template hosted on GitLab: 

`cookiecutter https://gitlab.com/mikedoesdatascience/cookiecutter-python-package`

This command will launch a series of questions for you to answer about the name of your project, the repository that will host the project, a description, who you are, and what license you want to use. You answers will populate the relevant pieces of the template to customize it to your liking. Once you answer those questions, you'll see a new directory that has been created. Now you'll want to connect this to your remote git repository by entering this directory and using `git init` and `git remote add origin <remote url>`. Then you'll be able to commit this initial project structure to your remote repository. The rest of the sections below will explain what all this code is that now exists in your new project.


## Gitpod development environment

If you haven't heard of Gitpod already, you should [go read about it](https://www.gitpod.io/about). With just a configuration file in you git repository, you can launch a customizable development IDE in the cloud in a single click (or URL). If your project is hosted on GitLab, look for the Gitpod button to launch your ephemeral IDE:

![](images/gitpod-button.png)

Gitpod takes care of git credentials and the instance comes pre-installed with Python and more importantly Docker, complete with terminal access. You can even [customize the Docker image you want to use](https://www.gitpod.io/blog/docker-in-gitpod). Best of all, it's free up to a certain usage per month so you can try it out before deciding whether it's worth paying if your usage surpasses their free quota.

Gitpod looks for a `.gitpod.yml` file in the root of your repository to know what to do. This is where you can add in all sorts of customizations - our cookiecutter template simply contains a single task that will set up and install some development tools and install the python package we're working on locally.

```bash
tasks:
  - name: Setup dev environment
    before: |
      alias ltr='ls -lhtr'
      echo "alias ltr='ls -lhtr'" >> ~/.bashrc
      python -m pip install -U pip
      python -m pip install bump2version black build twine
      python -m pip install -e ./python
```

The above sets my favorite bash alias for listing files at the terminal, updates `pip` and installs `bump2version` (explained later), `black`, and installs tools for publishing our package to PyPi. You'll see later that publishing will take place in a GitLab CI/CD job, so strictly speaking it's not necessary locally here. Lastly, the python project that we're working on will be install locally in editable mode. In reality, our goal is to use Docker to containerize our project, but installing locally, with all dependencies, will help make our IDE even smarter and help with autocomplete, documentation, and things of that nature.

## The Makefile

The `Makefile` contains a few commands useful for the devops life cycle. The first one `pip-lock` will be explained in the next section, but there are other commands for building and pushing docker images, and some shortcuts for running or getting a terminal in the Docker image we'll build.


## Python dependencies with pip locking

Despite all of the great things about python, one of the aspects that still can be very frustrating is handling the dependency hell that arises because python is not a complied language. Linux containers offer a very powerful solution for environment isolation that can package applications into portable workflows. I won't get into a debate here about `venv` vs `conda` vs `Docker`, because ultimately it mostly comes down to personal preference. However, the one technical aspect I will highlight is that while all three define _recipes_ for how to create a reproducible environment (`requirements.txt`, `environment.yaml` and `Dockerfile`), the resulting environments themselves for virtual or conda environments are not portable in the same way a Docker image is. There will always be edge cases, but you should expect a higher likelihood of success for sharing a Docker image with your colleague than you might expect if you were to tar a conda environment and ship it over, or if you were to share the environment yaml file and ask them to recreate the environment.

Given that you're on board with Docker containers at this point, the next issue I want to address is regarding the python dependencies themselves. While I just tried to make a strong case to focus on the Docker image as the final product rather than the Dockerfile recipe, we still want to be able to rebuild the same exact environment next week when we come back to work on our project, or next month, or next year. The natural solution is to pin every version number in your python `requirements.txt` file, which is indeed part of the solution I'll present here. But, in addition, we want to be able to quickly update our environment to make use of the newest features of our package's dependencies. Let's take, for example, a use case where I am building an application that uses `scikit-learn`. As of the writing of this blog post, if I install with `python -m pip install scikit-learn`, and ask pip to freeze the environment with `python -m pip freeze` I am given the following:

```bash
joblib==1.2.0
numpy==1.24.3
scikit-learn==1.2.2
scipy==1.10.1
threadpoolctl==3.1.0
```

Let's time travel a bit, and pretend that a year from now there's a shiny new feature in a new version of `scikit-learn` that I want to use, and this above was the `requirements.txt` file that I've recorded to recreate reproducible environments. I happen to know that in this small project I only installed `scikit-learn` and everything else was installed as a dependency, but knowledge of what was installed as a direct dependency of my project (i.e., something I import directly), or what was installed as an implicit dependency of my direct dependency is missing. How do I know what version to bump `numpy` to if I bump `scikit-learn` to a future `1.3.x` version? What if this new version of `scikit-learn` consequently depends on a specific version of `numpy` and is incompatible with `numpy==1.24.3`? Rather than introducing these uncertainties, there is a better solution that's common in other languages like nodejs with a `package.json` and a `package-lock.json`.

A simple solution implemented in our cookiecutter template is to use a "locking" mechanism, implemented through Docker builds, that maintains both a list of direct "development" dependencies, optionally pinned to whatever versions are necessary (but ideally unpinned) as well as a list of pinned dependencies used to create our reproducible environments. "Locking" is done by taking the development dependencies, and in a fresh Linux container installing them with their implicit dependencies and then asking this fresh environment to freeze dependencies. This is then what gets used at build time for our application which should be fully reproducible. If we consider our time traveling example again where in the future we read about new shiny features, we simply re-run our pip locking which takes our development dependencies and recreates a pinned set of dependencies frozen in time.

That's enough words - let's get to an example. In the repository we created with cookiecutter, you will see a "python" subdirectory that contains a `requirements.txt` file. Let's populate this with what I referred to as "development" dependencies, i.e. - unpinned python packages we will want to import in our project. These are entered just 1 per line, for example:

```bash
# requirements.txt
matplotlib
numpy
pandas
scikit-learn
seaborn
```

Let's see what happens when we run the make target `make pip-lock`. As you follow the output at your terminal, you'll see something running a pip install and you'll recognize the package names as those we just defined. If you take a look inside the python directory, you'll see a new file `requirements.lock` complete with all the implicit dependencies and pinned versions that capture the reproducible environment we will want to build. 

```bash
# requirements.lock
contourpy==1.0.7
cycler==0.11.0
fonttools==4.39.4
joblib==1.2.0
kiwisolver==1.4.4
matplotlib==3.7.1
numpy==1.24.3
packaging==23.1
pandas==2.0.2
Pillow==9.5.0
pyparsing==3.0.9
python-dateutil==2.8.2
pytz==2023.3
scikit-learn==1.2.2
scipy==1.10.1
seaborn==0.12.2
six==1.16.0
threadpoolctl==3.1.0
tzdata==2023.3
```

A year from now, not only do we have this requirements file with explicit pinned dependencies allowing us to recreate the environment, we also have the loosely defined set of requirements that we can use to redefine these explicit dependencies frozen in time.

This workflow is implement by using a fresh Docker base image to install from the development dependencies (`requirements.txt`) defined in `docker/Dockerfile.lock`. The command of this docker image will print the output of `python -m pip freeze`.

```docker
FROM python:3.11-slim

RUN python -m pip install -U pip

COPY python/requirements.txt /tmp/requirements.txt

RUN python -m pip install -U --no-cache-dir -r /tmp/requirements.txt

CMD ["python", "-m", "pip", "freeze"]
```

We use the make target `pip-lock` to build this locking docker image, and then run it to capture this output and write it to the new `requirements.lock` file.

```bash
pip-lock:
	@docker build \
		--no-cache \
		-t $(REGISTRY):lock \
		-f docker/Dockerfile.lock \
		.
	@docker run -it --rm $(REGISTRY):lock > python/requirements.lock
```

Then, when we make our real docker image for our application using `docker/Dockerfile` instead of using `requirements.txt` (not reproducible) we use this locked set of dependencies `requirements.lock`.

```bash
FROM python:3.11-slim

RUN python -m pip install -U pip

COPY python/requirements.lock /tmp/requirements.lock

RUN python -m pip install -U --no-cache-dir -r /tmp/requirements.lock && \
    rm /tmp/requirements.lock

...
```

You can use either of these files to dynamically define dependencies in your python package itself (outside of the Docker context). Since I tend to use Docker for complete reproducibility, I simply use the development dependencies in `pyproject.toml` with the justification that I would like to rely on pip to solve the complex nature of the dependency graph if you install my package in combination with other python packages.

```bash
[project]
dynamic = ["dependencies"]

[tool.setuptools.dynamic]
dependencies = {file = ["requirements.txt"]}
```

In other words, if you plan to use my _application_ I expect you to use my Docker image; if you plan to _develop something new_ using my package as a dependency, you must assume some responsibility to go through this same process to define your own reproducible environments, and I don't want to over specify dependencies that cause unnecessary headaches.


## Package versioning with bump2version

This section and the next deal with publishing artifacts (PyPi packages, Docker images) when it comes time to release your code/application. Just like we as developers like that `numpy` and `scikit-learn` and the likes deal with versioning their packages, we should aim to do the same. This also applies for versioning experiments, for example machine learning (ML) experiments, as a specific version may relate to a certain ML architecture or snapshot of data available at a certain time. For this we use `bump2version`, a simple command line tool to track and modify the existence of version numbers in our codebase.

The configuration file `.bumpversion.cfg` looks like the following:

```bash
[bumpversion]
current_version = 0.0.1
commit = true
parse = (?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(\-(?P<release>[a-z]+)(?P<build>\d+))?
serialize = 
	{major}.{minor}.{patch}-{release}{build}
	{major}.{minor}.{patch}

[bumpversion:file:python/{{ cookiecutter.package_name }}/__init__.py]
[bumpversion:file:python/pyproject.toml]

[bumpversion:part:release]
optional_value = prod
first_value = dev
values = 
	dev
	prod

[bumpversion:part:build]
```

When you create your own project using cookiecutter, `{{ cookiecutter.package_name }` will be replaced with your own package name. The 2 lines that begin with `bumpversion:file` define files that contain our version number - the `__init__.py` file in our project, and our `pyproject.toml` package configuration file. Our version numbers will follow the pattern `<major>.<minor>.<patch>` with optional tagging of dev versions with different builds, i.e. `1.0.3-dev5` would be the development work (iteration 5) towards the `1.0.3` release. The use of these version numbers would fall under the category of business logic defined for your project, but that pattern should be flexible enough to cover most use cases.

Using `bump2version` is as easy as running `bump2version <part to bump>` where `<part to bump>` takes the form of `major`, `minor`, `patch`, `release`, or `build`. We configured `bump2version` to additionally make a git commit to capture the increase the the appropriate part of the version number. For example, we start with version `0.0.1` and if we run `bump2version patch` we will en up with `0.0.2-dev0`. After some hard work, when we're ready to release version `0.0.2` we would run `bump2version release` to get to version `0.0.2`.

## GitLab CI/CD

The last piece of the puzzle is automation. Once we get to the point that we're ready to publish our application, we will make use of GitLab CI/CD to build our PyPi package as well as a docker image. The `.gitlab-ci.yml` file is what controls this part, and can be seen below:

```bash
stages:
  - publish
  - build

pypi-publish:
  stage: publish
  image: {{ cookiecutter.base_docker_image }}
  only:
    - tags
  needs: []
  script:
    - python -m pip install twine
    - cd python
    - python setup.py sdist bdist_wheel
    - python -m twine upload -u __token__ -p $PYPI_TOKEN --verbose dist/*

docker-build-push:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  only:
    - tags
  needs: ["pypi-publish"]
  before_script:
    - apk add make python3 py-pip
    - docker info
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - make build-prod
    - make build-prod VERSION=latest
    - make push
    - make push VERSION=latest
```

There are 2 stages `publish` and `build` that refer to publishing to PyPi and building/pushing to a docker registry. As these require authentication and authorization credentials, these are defined as [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/) for your project. These CI/CD jobs are only triggered on `tags`, so controlling who has the ability to release your software should be done through managing permissions to create tags.

The PyPi build is [straightforward](https://packaging.python.org/en/latest/tutorials/packaging-projects/), but there is one nuance worth mentioning with the publishing the Docker image. Locally when we build our Docker image we copy source files from our local filesystem during build time. If someone then wanted to use our published Docker image as a base image for their follow-up work, knowledge of where our python package came from would be lost. To remedy this, at publishing time, we first publish our python package to PyPi, and use yet a third variant of our Dockerfile `docker/Dockerfile.prod` that still uses the same `requirements.lock` file for reproducibility of _our_ dependencies, but uses the remote version of our package on PyPi to define the dependency for anything that wants to build on top of our package.


## Conclusion

So there you have it - a quick and easy way to get up and running with a lot of devops tooling applicable across all sorts of python projects. As you can see, there is quite a lot of code structure outside of the application logic itself. `cookiecutter` offers a convenient way to scaffold this so getting started on a new project can be a breeze. Feel free to take this, extend upon it, and share what things you see that are missing from your devops workflows!